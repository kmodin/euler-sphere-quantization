# Long time numerical simulation of ideal hydrodynamics on rotating and non-rotating spheres

This page contains material related to [this paper](http://doi.org/10.1017/jfm.2019.944) published open access in the Feb 2020 issue of the Journal of Fluid Mechanics. 
The paper is about the long time, structure preserving integration of the incompressible Euler equations on a sphere.

## Movies

A compressed file with all the movies can be [downloaded here](https://chalmersuniversity.box.com/s/5nmhred5aeb7guld7m992v9v6vhmi1qj), or they can be viewed individually by pressing the links below.

- [Movie 1](https://chalmersuniversity.box.com/s/b4rgzqj2mvjy24qzjqc3jz4hxfjtc5h8): Initial vortex mixing phase for the non-rotating sphere with randomly generated initial conditions with vanishing total angular momentum. The inverse energy cascade of stretching and thinning keeps on going until the vorticity is concentrated in two negative and two positive vortex blobs.

- [Movie 2](https://chalmersuniversity.box.com/s/gruvda25155c6aqdgapyiq676lshsoxo): A fast-forward of the non-rotating sphere with randomly generated initial conditions with vanishing total angular momentum. After a short emerging phase of vortex mixing, the four vortex blobs move about each other in a quasi-periodic fashion, with no indication of further mixing or of reaching steadyness.

- [Movie 3](https://chalmersuniversity.box.com/s/5sa4vy2l7opmme6mvbl3fm8kdsp9ll9s): The same non-rotating sphere example as in Movie 1 and Movie 2, but now using a four times higher spatial resolution. The qualitative behavior is the same, with the formation of four vortex blobs reaching a quasi-periodic asymptotic.

- [Movie 4](https://chalmersuniversity.box.com/s/g4tecvhety49121u9mcdzzvlzuc28zpe): A non-rotating sphere with initial configuration given by Gaussian blobs in accordance with a position configuration of the vortex blobs formation in Movie 1 and Movie 2. The spatial resolution here is very low (only 1/100 of the resolution in Movie 1). Nevertheless, the same asymptotic behavior is observed. This is important, as it allows us to study very long vortex blob simulations (thousands of revolutions) without extensive computational resources.

- [Movie 5](https://chalmersuniversity.box.com/s/39a0ku17atszbz9nc3tc8k93zldgmlsy): Simulation 1 from 16 simulations with generic initial conditions (non-zero momentum). This simulation shows the formation of two coherent vortex structures.

- [Movie 6](https://chalmersuniversity.box.com/s/dtimn4mf8nuj0fq9mjlc7m4s5vrjocp0): Simulation 4 from 16 simulations with generic initial conditions (non-zero momentum). This simulation shows the formation of three coherent vortex structures.

- [Movie 7](https://chalmersuniversity.box.com/s/89a78bto572cncpx9jy19aav43icq6h6): Simulation 7 from 16 simulations with generic initial conditions (non-zero momentum). This simulation shows the formation of four coherent vortex structures.

- [Movie 8](https://chalmersuniversity.box.com/s/acgzitxaabkkge871xaalb2menr1ejzx): A rotating sphere with initial conditions corresponding to an unstable Rossby-Haurwitz wave. Eventually, due to numerical rounding errors, the wave breaks up, undergoes a transition phase, and then settles on a quasi-periodic zonal motion, with a northerly zonal vortex street similar to those regularly seen on Jupiter.

---

## MATLAB source code

We are currently working with a refurnished and better documented version of our code to be made publicly available here. 
In the meantime, if you like to get your hands on the code before that, please contact either Klas Modin or Milo Viviani (emails available in the PDE of the [JFM publication](http://doi.org/10.1017/jfm.2019.944)).